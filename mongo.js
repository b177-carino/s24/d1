// Query Operator

// [SECTION] Comparison Query Operators
// $gt and $gte operator (Greater than/ Greater than or equal to)

// find users with an age greater than 50

db.users.find({
	age: {
		$gt:50
	}
})

// find users with an age greater than or equal to 50

db.users.find({
	age: {
		$gte:50
	}
})

// find users with an age less than or equal to 50

db.users.find({
	age: {
		$lte:50
	}
})

// find users with an age that is NOT equal to 82

db.users.find({
	age: {
		$ne: 82
	}
})

// find users whose last names are either "Hawking" or "Doe"
db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
})

// find users whose courses including "HTML" or "React"

db.users.find({
	courses: {
		$in: ["HTML", "React"]
	}
})

// [SECTION] Logical Query Operators

// $or operator (good for different fields)
db.users.find({
	$or: [
		{
			firstName: "Neil"
		},
		{
			age: 21
		}
	]
})

db.users.find({
	$or: [
		{
			lastName: "Hawking"
		},
		{
			lastName: "Doe"
		}
	]
})

// $and operator
db.users.find({
	$and: [
	{
		age: {
			$ne:82
		}
	 },
	 {
	 	age:{
	 		ne: 76
	 	}
	 }	
	]
})